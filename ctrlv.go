/*
 * Copyright (c) 2014 Keno Schwalb
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

// Package ctrlv implements an easy interface to the CtrlV platform
package ctrlv

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
)

type paste struct {
	Syntax   string `json:"syntax"`
	Expires  string `json:"expires"`
	Redirect string `json:"redirect"`
	Content  string `json:"content"`
}

// NewPaste creates a new paste on the CtrlV server at http://ctrlv.zsi.li
// see http://ctrlv.zsi.li for a list of supported syntax and epxires strings.
func NewPaste(syntax string, expires string, content string) (url string, err error) {
	paste := &paste{}
	paste.Syntax = syntax
	paste.Expires = expires
	paste.Content = content
	paste.Redirect = "false"

	body, err := json.Marshal(paste)
	if err != nil {
		return
	}

	req, err := http.NewRequest("POST", "http://ctrlv.zsi.li/submit", bytes.NewReader(body))
	if err != nil {
		return
	}

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return
	}

	urlByte, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}

	url = string(urlByte)

	if url == "invalid parameters" {
		err = io.EOF
	}

	return
}
