# ctrlv

    import "0x.cx/caerostris/ctrlv-go"

Package ctrlv implements an easy interface to the CtrlV platform

## Usage

#### func  NewPaste

```go
func NewPaste(syntax string, expires string, content string) (url string, err error)
```
NewPaste creates a new paste on the CtrlV server at http://ctrlv.zsi.li see
http://ctrlv.zsi.li for a list of supported syntax and epxires strings.
